export const Colors = {
  border: '#444',
  themeBackground: '#212526',
  themeGrayLight: '#988',
  themeGray: '#363333',
  themeGray2: '#555',
  themeGrayDark: '#2a2e31',
  themeOrangeLight: '#ff9040',    
  themeOrange: '#bb5500',
  themeOrangeDark: '#993000',
};

export const Icons = {
  speaker: 'volume-up',
  audioFile: 'file-audio-o',
  moreOptions: 'ellipsis-v',
  play: 'play',
  pause: 'pause',
  stop: 'stop',
  repeat: 'refresh',
  menu: 'bars',
  changeDirectory: 'folder'
};

export const Labels = {
  groovyPlayer: 'Groovy Player',
  recentlyPlayed: 'Recently played',
  rhythmsList: 'Rhythms\' list',
  rhythmDetails: 'RhythmDetails',
  drawerToggle: 'DrawerToggle'
};

export const Fake = {
  rhythmName1: 'Djaa',
  rhythmName2: 'Madan',
  rhythmName3: 'Soli',
  trackName1: 'Djembe solo',
  trackName2: 'Sangban',
  trackName3: 'Dununba'
}