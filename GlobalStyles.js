import { StyleSheet } from 'react-native';
import { Colors } from './Constants';

export default StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'stretch',
    backgroundColor: Colors.themeBackground,
  },
  text: {
    color: Colors.themeGrayLight
  },
  row: {
    flexDirection: 'row'
  },
  spBet: {
    justifyContent: 'space-between',
  },
  spAr: {
    justifyContent: 'space-around',
  },
  mtop10: {
    marginTop: 10
  },
  shadow: {
    elevation: 3,
    flex: 1,
    borderWidth: 3,
    borderColor: Colors.border
  },
  headerIcon: {
    fontSize: 24,
    paddingLeft: 15,
    paddingRight: 15
  },
  flex: {
    flex: 1
  },
  flex1p7: {
    flex: 1.7
  },
  navigationHeader: {
    padding: 20,
    paddingTop: 120,
    backgroundColor: Colors.themeOrange
  },
  navigationHeaderText: {
    fontSize: 24,
    fontWeight: 'bold'
  },
  navigationRecentHeaderText: {
    padding: 20,
    paddingTop: 20,
    borderBottomWidth: 1,
    borderColor: Colors.border,
    backgroundColor: Colors.border
  },
  navigationRecentItem: {
    padding: 20,
    borderBottomWidth: 1,
    borderColor: Colors.border
  },
  navigationRecentItemText: {
    color: Colors.themeGrayLight
  }
});