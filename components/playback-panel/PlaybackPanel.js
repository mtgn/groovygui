import React, { Component } from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  Slider,
} from 'react-native';
import RhythmList from '../rhythm-list/RhythmList';
import styles from '../../GlobalStyles';
import componentStyles from './styles';
import Icon from 'react-native-fa-icons';
import { Colors, Icons } from '../../Constants';

export default class PlaybackPanel extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tempo: 120,
      isPlaying: false,
      isPaused: false,
      isRepeating: false,
      isBpmSliderVisible: false,
    };

    this.onBpmSliderChange = this.onBpmSliderChange.bind(this);
    this.onPlayPress = this.onPlayPress.bind(this);
    this.onStopPress = this.onStopPress.bind(this);
    this.onRepeatPress = this.onRepeatPress.bind(this);
    this.onBpmPress = this.onBpmPress.bind(this);
  }

  render() {
    return (
      <View style={[styles.container, componentStyles.playbackPanelContainer]}>

        <View style={[styles.row, styles.spBet, componentStyles.timelineRow, styles.mtop10]}>
          <Text style={componentStyles.timeText}>0:00</Text>
          <Slider minimumValue={0} maximumValue={160} style={componentStyles.slider} step={1} thumbTintColor={Colors.themeOrangeLight} minimumTrackTintColor={Colors.themeOrangeDark} />
          <Text style={componentStyles.timeText}>2:40</Text>
        </View>

        <View style={[styles.row, styles.spBet, componentStyles.buttonsRow, styles.mtop10]}>

          <View style={[styles.row, this.state.isBpmSliderVisible && componentStyles.tempoRow]}>
            <TouchableOpacity onPress={this.onBpmPress} style={[componentStyles.playbackButton, this.state.isBpmSliderVisible && componentStyles.activeButton]}>
              <Text>{this.state.tempo}</Text>
              <Text>BPM</Text>
            </TouchableOpacity>
            {this.state.isBpmSliderVisible &&
              <View style={{ flex: 1 }}>
                <Slider minimumValue={80} maximumValue={150} value={this.state.tempo} style={componentStyles.slider} onValueChange={this.onBpmSliderChange} step={1}thumbTintColor={Colors.themeOrangeLight} minimumTrackTintColor={Colors.themeOrangeDark} />
              </View>}
          </View>

          {!this.state.isBpmSliderVisible && [<TouchableOpacity key={Icons.play} onPress={this.onPlayPress} style={[componentStyles.playbackButton, this.state.isPaused && componentStyles.activeButton]}>
            <Icon name={this.state.isPlaying ? Icons.pause : Icons.play} style={componentStyles.playbackIcon} />
          </TouchableOpacity>,
          <TouchableOpacity key={Icons.stop} onPress={this.onStopPress} style={[componentStyles.playbackButton, !this.state.isPlaying && componentStyles.disabledButton]}>
            <Icon name={Icons.stop} style={componentStyles.playbackIcon} />
          </TouchableOpacity>,
          <TouchableOpacity key={Icons.repeat} onPress={this.onRepeatPress} style={[componentStyles.playbackButton, this.state.isRepeating && componentStyles.activeButton]}>
            <Icon name={Icons.repeat} style={componentStyles.playbackIcon} />
          </TouchableOpacity>]}

        </View>
      </View>
    );
  }

  onBpmSliderChange(newTempo) {
    this.setState({ tempo: newTempo });
  };

  onPlayPress(event) {
    if (this.state.isPlaying) {
      this.setState({ isPaused: !this.state.isPaused });
    } else {
      this.setState({ isPlaying: true });
    }
  };

  onStopPress(event) {
    this.setState({ isPlaying: false, isPaused: false });
  };

  onRepeatPress(event) {
    this.setState({ isRepeating: !this.state.isRepeating });
  };

  onBpmPress(event) {
    this.setState({ isBpmSliderVisible: !this.state.isBpmSliderVisible });
  };
}