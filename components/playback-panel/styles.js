import { StyleSheet } from 'react-native';
import { Colors } from '../../Constants';

export default StyleSheet.create({
  playbackPanelContainer: {
    flex: 1,
    backgroundColor: Colors.themeGrayDark,
    justifyContent: 'flex-start',
    alignItems: 'flex-end',
  },
  slider: {
    flex: 1
  },
  playbackButton: {
    width: 60,
    height: 60,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.themeOrange,
    padding: 10,
    margin: 10,
    borderRadius: 15,
    elevation: 3
  },
  timelineRow: {
    flex: 1,
    padding: 20,
    paddingTop: 25,
    marginBottom: 5,
    alignItems: 'center',
    alignSelf: 'center'
  },
  buttonsRow: {
    alignItems: 'center',
    justifyContent: 'center',
    padding: 20,
    paddingTop: 0,
  },
  activeButton: {
    backgroundColor: Colors.themeOrangeLight
  },
  disabledButton: {
    backgroundColor: Colors.themeGrayLight
  },
  playbackIcon: {
    fontSize: 24
  },
  tempoRow: {
    backgroundColor: Colors.themeOrange,
    margin: -20,
    padding: 20,
    paddingTop: 5,
    marginTop: -5
  },
  flex1: {
    flex: 1
  },
  flex3: {
    flex: 3
  },
  timeText: {
    color: Colors.themeGrayLight
  }
});