import React, { Component } from 'react';
import {
  Text,
  View,
  FlatList,
  TouchableOpacity
} from 'react-native';
import styles from '../../GlobalStyles';
import componentStyles from './styles';
import Icon from 'react-native-fa-icons';
import { Icons, Fake } from '../../Constants';

export default class RhythmList extends Component {
  constructor(props) {
    super(props);
    this.onPress = this.onPress.bind(this);
    this.onOptionsPress = this.onOptionsPress.bind(this);
  }
  render() {
    return (
      <View style={styles.container}>
        <FlatList
          data={this.rhythmNames}
          renderItem={({ item }) =>
            <View style={[styles.row, componentStyles.listItemContainer]}>
              <TouchableOpacity style={[componentStyles.listItem, styles.row]} onPress={generateHandler(item.key, this.onPress)}>
                <Icon name={Icons.audioFile} style={[componentStyles.icon, componentStyles.fileIcon]} />
                <Text style={componentStyles.listItemText}>
                  {item.key}
                </Text>
              </TouchableOpacity>
              <TouchableOpacity onPress={generateHandler(item.key, this.onOptionsPress)} style={{ flex: 1, padding: 24, justifyContent: 'center' }}>
                <Icon name={Icons.moreOptions} style={componentStyles.icon} />
              </TouchableOpacity>
            </View>
          }
        />
      </View>
    );
  }

  onPress(event, itemName) {
    this.props.onItemPressed(event, itemName);
  };

  onOptionsPress(event, itemName) {

  };

  rhythmNames = [{ key: Fake.rhythmName1 }, { key: Fake.rhythmName2 }, { key: Fake.rhythmName3 }];
}

// Helper method that returns a function
const generateHandler = (value, method) => e => method(e, value);