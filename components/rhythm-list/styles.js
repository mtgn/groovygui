import { StyleSheet } from 'react-native';
import { Colors } from '../../Constants';

export default StyleSheet.create({
  listItemContainer: {
    backgroundColor: Colors.themeGray,
    borderBottomWidth: 1,
    borderColor: Colors.border,
    alignItems: 'stretch',
    justifyContent: 'space-between'
  },
  listItem: {
    paddingTop: 24,
    paddingBottom: 24,
    paddingLeft: 20,
    justifyContent: 'space-between',
    alignItems: 'center',
    flex: 6,
  },
  listItemText: {
    flex: 3,
    fontSize: 16,
    color: Colors.themeGrayLight,
    marginLeft: 30,
    marginRight: 30
  },
  icon: {
    flex: 0,
    fontSize: 16,
    paddingLeft: 15,
    paddingRight: 15,
  },
  fileIcon: {
    fontSize: 32,
    color: Colors.themeGrayLight
  }

});