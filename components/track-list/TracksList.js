import React, { Component } from 'react';
import {
  Text,
  View,
  ScrollView,
  Slider,
  Switch
} from 'react-native';
import styles from '../../GlobalStyles';
import componentStyles from './styles';
import Icon from 'react-native-fa-icons';
import { Colors, Icons, Fake } from '../../Constants';

export default class TracksList extends Component {
  render() {
    return (
      <ScrollView>
        <View style={[componentStyles.tilesContainer]}>
          <View style={[componentStyles.tile, styles.row]}>
            <Switch value={true} />
            <Text style={componentStyles.tileText}>{Fake.trackName1}</Text>
            <Icon name={Icons.speaker} /><Slider style={componentStyles.slider} value={70} />
          </View>
          <View style={[componentStyles.tile, styles.row]}>
            <Switch value={true} />
            <Text style={componentStyles.tileText}>{Fake.trackName2}</Text>
            <Icon name={Icons.speaker} /><Slider style={componentStyles.slider} value={70} />
          </View>
          <View style={[componentStyles.tile, styles.row]}>
            <Switch tintColor={Colors.themeGrayLight} />
            <Text style={componentStyles.tileText}>{Fake.trackName3}</Text>
            <Icon name={Icons.speaker} /><Slider style={componentStyles.slider} value={70} />
          </View>
        </View>
      </ScrollView>
    );
  }
}