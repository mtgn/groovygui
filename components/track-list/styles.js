import { StyleSheet } from 'react-native';
import { Colors } from '../../Constants';

export default StyleSheet.create({
  tileContainer: {
    flex: 1,
    alignItems: 'center'
  },
  tile: {
    padding: 20,
    paddingTop: 27,
    paddingBottom: 27,
    backgroundColor: Colors.themeGray,
    justifyContent: 'space-between',
    alignItems: 'center',
    flex: 1,
    borderBottomWidth: 1,
    borderColor: Colors.border,
  },
  tileText: {
    marginLeft: 20,
    flex: 1,
    color: Colors.themeGrayLight
  },
  slider: {
    flex: 1
  }
});