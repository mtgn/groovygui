import { AppRegistry } from 'react-native';
import Navigation from './screens/Navigation';

AppRegistry.registerComponent('GroovyGUI', () => Navigation);
