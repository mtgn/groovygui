import React, { Component } from 'react';
import {
  TouchableOpacity,
  View,
  FlatList,
  Text,
  ScrollView
} from 'react-native';
import {
  StackNavigator,
  DrawerNavigator,
  DrawerItems
} from 'react-navigation';
import styles from '../GlobalStyles';
import Icon from 'react-native-fa-icons';
import RhythmListScreen from './rhythm-list-screen/RhythmListScreen';
import DetailsScreen from './details-screen/DetailsScreen';
import { Colors, Icons, Labels, Fake } from '../Constants'

const stackNavigationOptions = {
  headerStyle: {
    backgroundColor: Colors.themeGrayDark,
    padding: 20,
    paddingTop: 0,
    paddingBottom: 0,
  },
  headerTintColor: Colors.themeGray2,
  headerMode: 'screen',
};

const RhythmsNavigation = StackNavigator({
  RhythmList: {
    screen: RhythmListScreen,
    navigationOptions: {
      headerLeft: <TouchableOpacity onPress={this.onPress}><Icon name={Icons.menu} style={styles.headerIcon} /></TouchableOpacity>,
      headerRight: <TouchableOpacity onPress={this.onPress}><Icon name={Icons.changeDirectory} style={styles.headerIcon} /></TouchableOpacity>,
    }
  },
  RhythmDetails: {
    screen: DetailsScreen
  }
}, {
    cardStyle: { opacity: 1 },
    navigationOptions: stackNavigationOptions
  });

class RhythmsNavigationWrapper extends React.Component {
  constructor(props) {
    super(props);
    this.onMenuToggle = this.onMenuToggle.bind(this);
  }

  render() {
    return (
      <RhythmsNavigation screenProps={{ onMenuToggle: this.onMenuToggle }} />
    );
  }

  onMenuToggle(event) {
    this.props.rootNavigation.navigate(Labels.drawerToggle);
  }
}

const DrawerNavigation = DrawerNavigator({
  Rhythms: { screen: RhythmsNavigationWrapper },
  Settings: { screen: RhythmsNavigation }
}, {
    drawerWidth: 240,
    contentComponent: props => (
      <ScrollView style={styles.flex}>
        <View style={[styles.navigationHeader, styles.flex]}><Text style={styles.navigationHeaderText}>{Labels.groovyPlayer}</Text></View>
        <View style={styles.flex1p7}>
          <DrawerItems {...props} />
          <Text style={styles.navigationRecentHeaderText}>{Labels.recentlyPlayed}</Text>
          <FlatList data={[{ key: Fake.rhythmName1 }, { key: Fake.rhythmName2 }, { key: Fake.rhythmName3 }]} renderItem={({ item }) =>
            <View style={styles.navigationRecentItem}><Text style={styles.navigationRecentItemText}>{item.key}</Text></View>} />
        </View>
      </ScrollView>),
    drawerBackgroundColor: Colors.themeGray,
    contentOptions: {
      activeTintColor: Colors.themeOrange,
      inactiveTintColor: Colors.themeGrayLight,
    }
  });

export default class Navigation extends Component {
  render() {
    return (
      <DrawerNavigation />
    );
  }
}