import React, { Component } from 'react';
import { View } from 'react-native';
import styles from '../../GlobalStyles';
import PlaybackPanel from '../../components/playback-panel/PlaybackPanel';
import TracksList from '../../components/track-list/TracksList';

export default class DetailsScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      headerTitle: navigation.state.params.fileName,
    }
  }

  render() {
    return (
      <View style={[styles.container, styles.spAr]}>
        <TracksList />
        <PlaybackPanel />
      </View>
    );
  }
}