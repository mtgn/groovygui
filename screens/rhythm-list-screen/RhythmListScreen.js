import React, { Component } from 'react';
import { View } from 'react-native';
import RhythmList from '../../components/rhythm-list/RhythmList';
import styles from '../../GlobalStyles';
import { Labels } from '../../Constants';

export default class RhythmListScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      directory: {
        name: null,
        path: null
      },
      fileName: null,
      recent: [],
    }

    this.onItemPressed = this.onItemPressed.bind(this);
  }

  static navigationOptions = ({ navigation }) => {
    const params = navigation.state.params;
    return {
      headerTitle: params && params.directory ? params.directory.name : Labels.rhythmsList,
      drawerTitle: Labels.rhythmsList
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <RhythmList directory={this.state.directory.path} onItemPressed={this.onItemPressed} />
      </View>
    );
  }

  onItemPressed(event, itemName) {
    const { navigate } = this.props.navigation;
    navigate(Labels.rhythmDetails, { fileName: itemName });
  }
}
